# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/).

## [Unreleased]

## [2020.12.0]

## Changed
- regenerated avpr based on recent traits update

## [2020.07.0]

### Changed
- migrated to Apache [Avro](https://yeps.yaq.fyi/107)

## [2020.06.0]

### Added
- initial release

[Unreleased]: https://gitlab.com/yaq/yaqd-acton/-/compare/v2020.12.0...master
[2020.12.0]: https://gitlab.com/yaq/yaqd-acton/-/compare/v2020.07.0...v2020.12.0
[2020.07.0]: https://gitlab.com/yaq/yaqd-acton/-/compare/v2020.06.0...v2020.07.0
[2020.06.0]: https://gitlab.com/yaq/yaqd-acton/-/tags/v2020.06.0

