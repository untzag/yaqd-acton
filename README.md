# yaqd-acton

[![PyPI](https://img.shields.io/pypi/v/yaqd-acton)](https://pypi.org/project/yaqd-acton)
[![Conda](https://img.shields.io/conda/vn/conda-forge/yaqd-acton)](https://anaconda.org/conda-forge/yaqd-acton)
[![yaq](https://img.shields.io/badge/framework-yaq-orange)](https://yaq.fyi/)
[![black](https://img.shields.io/badge/code--style-black-black)](https://black.readthedocs.io/)
[![ver](https://img.shields.io/badge/calver-YYYY.0M.MICRO-blue)](https://calver.org/)
[![log](https://img.shields.io/badge/change-log-informational)](https://gitlab.com/yaq/yaqd-acton/-/blob/master/CHANGELOG.md)

yaq daemons for Acton Research Corporation instruments

This package contains the following daemon(s):

- https://yaq.fyi/daemons/acton-2150i